<?php 

if( ! function_exists( 'road_get_slider_setting' ) ) {
	function road_get_slider_setting() {
		$status_opt = array(
			'',
			__( 'Yes', 'beeta' ) => true,
			__( 'No', 'beeta' ) => false,
		);
		
		$effect_opt = array(
			'',
			__( 'Fade', 'beeta' ) => 'fade',
			__( 'Slide', 'beeta' ) => 'slide',
		);
	 
		return array( 
			array(
				'type' => 'checkbox',
				'heading' => __( 'Enable slider', 'beeta' ),
				'param_name' => 'enable_slider',
				'value' => true,
				'save_always' => true, 
				'group' => __( 'Slider Options', 'beeta' ),
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Items Large Screen', 'organicfood' ),
				'param_name' => 'item_large',
				'group' => __( 'Slider Options', 'organicfood' ),
				'value' => 5,
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Items Default', 'beeta' ),
				'param_name' => 'items',
				'group' => __( 'Slider Options', 'beeta' ),
				'value' => 5,
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Item Desktop', 'beeta' ),
				'param_name' => 'item_desktop',
				'group' => __( 'Slider Options', 'beeta' ),
				'value' => 4,
			), 
			array(
				'type' => 'textfield',
				'heading' => __( 'Item Small', 'beeta' ),
				'param_name' => 'item_small',
				'group' => __( 'Slider Options', 'beeta' ),
				'value' => 3,
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Item Tablet', 'beeta' ),
				'param_name' => 'item_tablet',
				'group' => __( 'Slider Options', 'beeta' ),
				'value' => 2,
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Item Mobile', 'beeta' ),
				'param_name' => 'item_mobile',
				'group' => __( 'Slider Options', 'beeta' ),
				'value' => 1,
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Navigation', 'beeta' ),
				'param_name' => 'navigation',
				'value' => $status_opt,
				'save_always' => true,
				'group' => __( 'Slider Options', 'beeta' ),
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Pagination', 'beeta' ),
				'param_name' => 'pagination',
				'value' => $status_opt,
				'save_always' => true,
				'group' => __( 'Slider Options', 'beeta' )
			),
			array(
				'type' => 'textfield',
				'heading' => __( 'Speed sider', 'beeta' ),
				'param_name' => 'speed',
				'value' => '500',
				'save_always' => true,
				'group' => __( 'Slider Options', 'beeta' )
			),
			array(
				'type' => 'checkbox',
				'heading' => __( 'Slider Auto', 'beeta' ),
				'param_name' => 'auto',
				'value' => false, 
				'group' => __( 'Slider Options', 'beeta' )
			),
			array(
				'type' => 'checkbox',
				'heading' => __( 'Slider loop', 'beeta' ),
				'param_name' => 'loop',
				'value' => false, 
				'group' => __( 'Slider Options', 'beeta' )
			),
			array(
				'type' => 'checkbox',
				'heading' => __( 'Slider center', 'beeta' ),
				'param_name' => 'center',
				'value' => false, 
				'dependency' => array(
				    'element' => 'loop',
				    'value' => array ('true'),
				    'not_empty' => true,
				),
				'group' => __( 'Slider Options', 'beeta' )
			),
			array(
				'type' => 'dropdown',
				'heading' => __( 'Effects', 'beeta' ),
				'param_name' => 'effect',
				'value' => $effect_opt,
				'save_always' => true,
				'group' => __( 'Slider Options', 'beeta' )
			), 
		);
	}
}